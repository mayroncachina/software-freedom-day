module.exports = {

  prompts: false,

  // These are variables will be accessible via our templates
  templateData: {

    // Conference info
    conf: {
      name: "Software Freedom Day Natal/RN",
      description: "Software Freedom Day evento de informática em Natal/RN realizado no dia 21/09/2013",
      date: "",
      price: "",
      venue: "IFRN",
      address: "Avenida Senador Salgado Filho, 1559, Tirol, Natal - RN, 59015-000",
      city: "Natal",
      state: "RN"
    },

    // Site info
    site: {
      theme: "yellow-swan",
      url: "http://sfd.potilivre.org",
      googleanalytics: "UA-44018970-1"
    },

    // Active sections on the website
    // to deactivate comment out with '//'
    // you can also change order here and it will reflect on page
    sections: [
      'about',
      'location',
      'speakers',
      'schedule',
      'sponsors',
      'partners'
      // 'contact'
    ],

    // Labels which you can translate to other languages
    labels: {
      about: "Sobre",
      location: "Local",
      speakers: "Palestrantes",
      schedule: "Programação",
      sponsors: "Organização",
      partners: "Parceiros",
      contact: "Contato"
    },


    // The entire schedule
    schedule: [
      {
        name: "Credenciamento",
        time: "8h00"
      },
      {
        name: "Mayron Cachina",
        photo: "themes/yellow-swan/img/palestrantes/mayron.jpg",
        bio: "Especialista em Desenvolvimento de Sistema Corporativos pela UNI-RN. Trabalha com desenvolvimento de sites e software há mais de 10 anos, atualmente criador de várias startups e voltado para o ensino de desenvolvimento mobile a comunidade.",
        company: "S@ites",
        link: {
          href: "http://twitter.com/mayroncachina",
          text: "@mayroncachina"
        },
        presentation: {
          title: "PotiLivre, uma comunidade de Software livre voltada para você!",
          description: "Conheçe Linux e software livre e não faz parte dessa comunidade? Venha conhecer um pouco de nosso trabalho e o que estamos fazendo para melhorar o software livre em Natal!",
          time: "09h00"
        }
      },
      {
        name: "Tiago Oliveira de Jesus",
        photo: "themes/yellow-swan/img/palestrantes/tiago.jpg",
        bio: "Desenvolvendo aplicações para web desde 2003. Atua atualmente na Hotline como analista de sistema prestando serviço para a Petrobras. Conhecimentos nas linguagens Java, C#, php, javascript, python, Objectice-C.",
        company: "Hotline",
        link: {
          href: "http://twitter.com/tiagojesusbr",
          text: "@tiagojesusbr"
        },
        presentation: {
          title: "Javascript do Navegador para o Servidor.",
          description: "Uma apresentação de aproximadamente 15 minutos, explicando como uma iniciativa de criar o Google Chrome, provocou uma mudança na web e como efeito colateral houve o surgimento do Node.js criado baseado na implementação do motor javascript do Chrome, o V8.",
          time: "09h30"
        }
      },      

   
      {
        name: "Marcel Ribeiro Dantas",
        photo: "themes/yellow-swan/img/palestrantes/marcel.png",
        bio: "Pesquisador em Engenharia Biomédica no Laboratório de Inovação Tecnológica em Saúde (LAIS-HUOL) e aluno de Engenharia de Computação e Automação na UFRN, Marcel concilia suas atividades com o ativismo de software livre há quase uma década. Já ministrou palestras em universidades federais e eventos em vários estados do Brasil. Co-fundador da comunidade PotiLivre no Rio Grande do Norte, atualmente é embaixador do Fedora e membro de diversos times no Projeto Fedora como Marketing, Tradução, Freemedia e Embaixadores.",
        company: "",
        link: {
          href: "http://mribeirodantas.fedorapeople.org/",
          text: "~mribeirodantas"
        },
        presentation: {
          title: "Contribuições do Fedora à comunidade Open Source dos dias de hoje",
          description: "Sendo um sistema operacional upstream, o Fedora e seus desenvolvedores atuam diretamente nos softwares contidos na distribuição de modo que ao release de cada versão, o Fedora acaba por ter interagido com centenas de softwares e tecnologias na comunidade FOSS. Nesses dez anos que estamos prestes a completar, o Fedora teve participação significativa em vários projetos como o Linux, o Gnome, drivers livres para placas gráficas (nouveau/radeon) entre muitos outros, gerando uma revolução positiva na comunidade." ,
          time: "10h00"
        }
      }, 


      {
        name: "Alex Aquino dos Santos ",
        photo: "themes/yellow-swan/img/palestrantes/alex.jpg",
        bio: "Acadêmico do 6º período do curso de Ciência da Computação da Universidade do Estado do Rio Grande do Norte (UERN), membro do GIC (Grupo de Inteligência Computacional / UERN), bolsista no projeto de pesquisa de Desenvolvimento de um Sistema Multi-cellbots Inteligentes (CNPq / PIBIC), voluntário no projeto Navau X80 e criador do KEOMA Dog Robot (Plataforma Robótica de Baixo Custo em desenvolvimento como alternativa as principais plataformas comerciais existentes). Experiência em plataforma Arduino e desenvolvimento de robôs móveis.",
        company: "",
        link: {
          href: "",
          text: ""
        },
        presentation: {
          title: "Arduino, do Open Source e Open Hardware.",
          description: "O arduino tornou a prototipação eletrônica fácil e acessível. Através desse recurso é possível tirar a interatividade da telinha do computador e trazer para o mundo real, pondo-a em ambientes e objetos físicos. Nesta palestra apresentarei um tutorial sobre como começar com Arduino, bem como um overview das possibilidades em termos de aplicações." ,
          time: "11h00"
        }
      },            
      
      {
        name: "MESA REDONDA",
        time: "12h00"
      },


 {
        name: "Isaac Franco Fernandes",
        photo: "themes/yellow-swan/img/palestrantes/isaac.jpg",
        bio: "Engenheiro de Computação - UFRN / Mestre em Engenharia de Produção com ênfase em pesquisa operacional, otimização e computação de alta performance. / Doutorando em Engenharia de Computação. / Fundador da Sync Sistemas",
        company: "Sync Sistemas",
        link: {
          href: "http://twitter.com/isaac_franco ",
          text: "@isaac_franco"
        },
        presentation: {
          title: "Software Livre e Computação de Alta Performance",
          description: "Será apresentado o estado da arte dos supercomputadores utilizados para simular eventos climáticos, física de partículas, sistemas biológicos e até o pensamento humano. Serão mostradas as tecnologias são utilizadas, que aplicações são exploradas e como o software livre derrubou a hegemonia dos sistemas fechados e soluções de grandes empresas no desenvolvimento dos supercomputadores." ,
          time: "13h00"
        }
      },



      {
        name: "Luan Fonseca",
        photo: "themes/yellow-swan/img/palestrantes/luan.jpg",
        bio: "21 anos, Graduando em Engenharia de Software pela UFRN, Desenvolvedor de Software, na Multmeio e 4soft (Empresa Júnior do curso de Engenharia de Software)",
        company: "",
        link: {
          href: "",
          text: ""
        },
        presentation: {
          title: "Como participar de projetos Open Source utilizando Python",
          description: "Palestra com foco no modo de inserção de qualquer programador, que tenha o mínimo conhecimento, em projetos de código aberto e da comunidade ao seu redor, desmistificando a ponte que existe entre Programador x Projetos de Software Livre, focados na comunidade Python do Brasil e do Mundo." ,
          time: "14h00"
        }
      },



   {
        name: "Eduardo Coelho",
        photo: "themes/yellow-swan/img/palestrantes/eduardo.jpg",
        bio: "Professor do IFRN. Trabalhou com consultoria e implementação em projetos de infraestrutura e redes de computadores. Usuário de softwares opensource desde 1999. Utiliza o Moodle desde 2008 e desenvolve trabalho de mestrado na área de educação à distância.",
        company: " IFRN - Professor de Redes de Computadores",
        link: {
          href: "http://twitter.com/eduardocoelho",
          text: "@eduardocoelho"
        },
        presentation: {
          title: "Educação à distância, Moodle e Opensource",
          description: "Educação à distância é uma das área em que o opensource faz bonito e ocupa uma larga fatia do marketshare de ambientes virtuais de aprendizagem. O objetivo desta palestra é apresentar o atual cenário de ambientes virtuais de aprendizagem, o papel que o projeto opensource Moodle desempenhou e desempenha no desenvolvimento desta área que envolve principalmente educação e tecnologia da informação, e apresentar perspectivas para quem deseja iniciar ou se aperfeiçoar no uso deste tipo de ferramenta, seja você docente, profissional de TI ou estudante.",
          time: "15h00"
        }
      },  

      {
        name: "Leonardo Ataide Minora",
        photo: "themes/yellow-swan/img/palestrantes/minora.jpg",
        bio: "Desenvolvo software desde de meados de 1993, passando por diversas tecnologias e linguagens de programação. Atualmente sou professor do IFRN desde de 2003, onde eu tenho ministrado diversas disciplinas sobre desenvolvimento de software, coordenado diversos projetos de pesquisa e extensão, e ainda participado/contribuído em diversas comunidades (SL, JavaRN, +Web, 4Mobile, SPB, etc).",
        company: "",
        link: {
          href: "",
          text: ""
        },
        presentation: {
          title: "Oportunidade de contribuição no desenvolvimento e de negócio",
          description: "Criado em 12 de abril de 2007, o portal do SPB já conta com mais de 60 soluções voltadas para diversos setores. Os serviços disponíveis são acessados até por outros países, como Uruguai, Argentina, Portugal, Venezuela, Chile e Paraguai. Para a SLTI, o portal já se consolidou como um ambiente de compartilhamento de softwares. Estes softwares, ora publicados no Portal do Software Público Brasileiro, são oportunidades tanto para desenvolvedores de software bem como para empreendedores. Quer saber como, participe desta palestra :D" ,
          time: "16h00"
        }
      },  

      
      {
        name: "MESA REDONDA",
        time: "17h00"
      },



    ],

    // List of Sponsors
    sponsors: [
      {
        name: "PotiLivre",
        logo: "themes/yellow-swan/img/Ptileft.png",
        url: "http://potilivre.org"
      }
    ],

    // List of Partners
    partners: [
      {
        name: "IFRN",
        logo: "themes/yellow-swan/img/logo-ifrn.jpg",
        url: "http://www.ifrn.edu.br/"
      },
      {
        name: "S@ites",
        logo: "themes/yellow-swan/img/saites.png",
        url: "http://saites.com.br"
      },
      {
        name: "DevCast",
        logo: "themes/yellow-swan/img/devcast.png",
        url: "http://facebook.com/devcast/"
      },
      {
        name: "PSL-RN",
        logo: "themes/yellow-swan/img/pslrn.jpg",
        url: "http://rn.softwarelivre.org/"
      },
      {
        name: "GDG Natal",
        logo: "themes/yellow-swan/img/gdg.svg",
        url: "https://plus.google.com/111142392900301029730/"
      }
    ],

    // Theme path
    getTheme: function() {
      return "themes/" + this.site.theme;
    }
  }
};
