# Site Software Freedom Day Natal/RN

Baseado no Conf Boilerplate [![Build Status](https://secure.travis-ci.org/braziljs/conf-boilerplate.png?branch=master)](https://travis-ci.org/braziljs/conf-boilerplate)

## Configuração

Pré-requisitos: Instale o [Git](http://git-scm.com/downloads) e o [NodeJS](http://nodejs.org/download/), caso você não os tenha ainda.

1. Instale [Git](http://git-scm.com/downloads) e [NodeJS](http://nodejs.org/download/), caso você não os tenha ainda.

2. Abra seu terminal e instale o [DocPad](https://github.com/bevry/docpad) através do comando:

    [sudo] npm install -fg docpad@6.30

3. Clone o repositório:

    git clone git://github.com/braziljs/conf-boilerplate.git

4. Vá para pasta do projeto:

    cd conf-boilerplate

5. Instale todas as dependências:

    docpad install

6. E finalmente rode:

    docpad run

Agora você irá ver o site rodando em `localhost:9778`

## Licença

[MIT License](http://braziljs.mit-license.org/)
